import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QLineEdit, QLabel
import requests
import json 


class Window1(QWidget):
    def __init__(self):
        super(Window1, self).__init__()
        self.button = QPushButton('Button', self)
        self.setWindowTitle('Weather')
        self.setMinimumWidth(500)
        self.setMinimumHeight(500)
        self.button.setText('Get the forecast')
        self.button.show()

        self.ledit = QLineEdit(self)
        self.ledit.resize(200, 50)
        self.ledit.move(100, 90)
        self.ledit.setText('Saint Petersburg')
        self.ledit.setMaxLength(25)
        self.ledit.setStyleSheet("font: Verdana; font-size:50 px")
        self.ledit.show()
   
                   
class Window2(QWidget):
    def __init__(self):
        super(Window2, self).__init__()
        self.setWindowTitle('The forecast')
        self.label_w = QLabel("Weather: ", self)
        self.label_w.adjustSize()
        self.setMinimumWidth(500)
        self.setMinimumHeight(500)
                
         
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('Weather')
        
    def show_window_1(self):
        self.w1 = Window1()
        self.w1.button.clicked.connect(self.show_window_2)
        self.w1.button.clicked.connect(self.weather)
        self.w1.button.clicked.connect(self.w1.close)
        self.w1.show()
    
    def show_window_2(self, info):
        self.w2 = Window2()
        self.label_w2 = QLabel(self.weather())
        self.w2.show()

    def weather(self):
        try:
            res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                params={'4778626': 'city_id', 'units': 'metric', 'lang': 'en', 'appid': '5d1c5370369029f2d3d9274729db73b2'})
            data = res.json()
            print("conditions:", data['weather'][0]['description'])
            print("temp:", data['main']['temp'])
            print("temp_min:", data['main']['temp_min'])
            print("temp_max:", data['main']['temp_max'])
            print("wind: m/s", data ['wind']['deg'])
            json = res.json()
            return json
        except KeyError:
            return "The weather is fine"    
            
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    w.show_window_1()
    sys.exit(app.exec_())