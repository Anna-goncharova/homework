import requests

def get_json(city_name):
 response = requests.get(
 "http://api.openweathermap.org/data/2.5/weather",
 params={
  "q": city_name,
  "units": "metric",
  "appid": "5d1c5370369029f2d3d9274729db73b2",
  }
 )
 json = response.json()
 return json

def desc(json):
 intro = "Weather in " + str(json['name']) + ": "
 weather_main = json['weather'][0]['main']
 weather_desc = json['weather'][0]['description']
 rain = "Rain"
 if rain in weather_desc or rain in weather_main:
  add = "It's raining."
  return intro, add + "\n"
 return intro, weather_main + "\n"


def get_temp(json):
 temp = json['main']['temp']
 if temp >= 25:
  forecast = "Temperature: " +  str(temp) + " С°" 
 elif temp <= 24 and temp > 20:
  forecast = "Temperature: " +  str(temp) + " С°" 
 elif temp <= 20 and temp > 10:
  forecast = "Temperature: " +  str(temp) + " С°"
 elif temp <=10 and temp > 0:
  forecast = "Temperature: " +  str(temp) + " С°"
 elif temp <= 0 and temp > -15:
  forecast = "Temperature: " +  str(temp) + " С°"
 elif temp >= -15 and temp < -30:
  forecast = "Temperature: " +  str(temp) + " С°"
 else:
  forecast = "Temperature: " +  str(temp) + " С°"
 
 return forecast + "\n"


def wind_direction(json):
 try:
  deg = json['wind']['deg']
  if deg > 330 or deg <= 30:
   direction = "Nord."
  elif deg > 30 and deg <= 60:
   direction = "Nord-east."
  elif deg > 60 and deg <= 120:
   direction = "East."
  elif deg > 120 and deg <= 150:
   direction = "South-east."
  elif deg > 150 and deg <= 210:
   direction = "South."
  elif deg > 210 and deg <= 240:
   direction = "South-west."
  elif deg > 240 and deg <= 300:
   direction = "West."
  else:
   direction = "Nord-west."
  return direction + "\n"
  
 except KeyError:
  return "The weather is fine\n"