from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
 def setupUi(self, MainWindow):
  MainWindow.setObjectName("MainWindow")
  MainWindow.setWindowModality(QtCore.Qt.NonModal)
  MainWindow.resize(500, 320)
  self.centralwidget = QtWidgets.QWidget(MainWindow)
  self.centralwidget.setObjectName("centralwidget")
  self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
  self.lineEdit.setGeometry(QtCore.QRect(10, 250, 281, 61))
  self.lineEdit.setTabletTracking(False)
  self.lineEdit.setObjectName("lineEdit")
  self.pushButton = QtWidgets.QPushButton(self.centralwidget)
  self.pushButton.setGeometry(QtCore.QRect(300, 250, 191, 61))
  self.pushButton.setObjectName("pushButton")
  self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
  self.textBrowser.setGeometry(QtCore.QRect(10, 10, 481, 231))
  self.textBrowser.setObjectName("textBrowser")
  MainWindow.setCentralWidget(self.centralwidget)

  self.retranslateUi(MainWindow)
  QtCore.QMetaObject.connectSlotsByName(MainWindow)

 def retranslateUi(self, MainWindow):
  _translate = QtCore.QCoreApplication.translate
  MainWindow.setWindowTitle(_translate("MainWindow", "Forecast"))
  self.lineEdit.setPlaceholderText(_translate("MainWindow", "Enter city name"))
  self.pushButton.setText(_translate("MainWindow"))