import requests, sys
from PyQt5 import QtWidgets
from mockup import Ui_MainWindow
from weather_new import *

class main(QtWidgets.QMainWindow):
 def __init__(self):
  super(main, self).__init__()
  self.ui = Ui_MainWindow()
  self.ui.setupUi(self)
  self.ui.pushButton.clicked.connect(self.btn_clk)


 def btn_clk(self):
  self.ui.textBrowser.clear()
  city = self.ui.lineEdit.text().strip()
  json = get_json(city)
  try:
   a, b = desc(json)
  except KeyError:
   self.ui.textBrowser.setPlainText("City " + city + " not found")
   return
  self.ui.textBrowser.insertPlainText(a)
  self.ui.textBrowser.insertPlainText(b)
  self.ui.textBrowser.insertPlainText(get_temp(json))
  self.ui.textBrowser.insertPlainText(wind_direction(json))

app = QtWidgets.QApplication([])
application = main()
application.show()

sys.exit(app.exec())