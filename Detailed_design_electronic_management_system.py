import sqlalchemy as sa
import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker


Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.now)


class Designer(Common, Base):
    __tablename__ = "designer"

    full_name = sa.Column(sa.String(), nullable=False)
    title = sa.Column(sa.String(), nullable=False)
    designing_company_name = sa.Column(sa.String(), nullable=False)
    date_of_review = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    document_name = sa.Column(sa.String(), nullable=False)
    comments = sa.Column(sa.Text, nullable=True)


class Document(Common, Base):
    __tablename__ = "document"

    designer_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Designer.__tablename__}.id"), nullable=False)
    title = sa.Column(sa.String(), nullable=False)
    date_of_upload = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    date_of_delete = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    comments = sa.Column(sa.Text, nullable=True)

class StorageManager(Common, Base):
    __tablename__ = "storage manager"

    full_name = sa.Column(sa.String(), nullable=False)
    date_of_review = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    
class Executor(Common, Base):
    __tablename__ = "executor"

    full_name = sa.Column(sa.String(), nullable=False)
    title = sa.Column(sa.String(), nullable=False)
    document_name = sa.Column(sa.String(), nullable=False)
    date_of_receipt = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    comments = sa.Column(sa.Text, nullable=True)

class Role:
    def __init__(self):
        self.engine = sa.create_engine("sqlite:///database2.sqlite")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()
        Base.metadata.create_all(self.engine)

    def get_review_table(self):
        return self.session.query(Table).all()

class DesignerRole(Role): 
    def register(self, full_name, title, designing_company_name):
        designer = Designer(
            full_name=full_name,
            title=title,
            designing_company_name=designing_company_name,
        )
        self.session.add(designer)
        self.session.commit()
        self.record = designer
    
    def document_registration(self, document_name):
        self.session.add(document_name)
        self.session.commit()

    def comments(self, comments):
        self.session.add(comments)
        self.session.commit()   


class StorageManagerRole(Role):
    def register(self, full_name):
        storage_manager = StorageManager(full_name=full_name)
        self.session.add(storage_manager)
        self.session.commit()
        self.record = storage_manager

    def cancel_document(self, id):
        document = self.session.delete(Document).where(Document.id == id).first()

    def comments(self, comments):
        self.session.add(comments)
        self.session.commit()      


class ExecutorRole(Role):
     def register(self, full_name):
        executor = Executor(full_name=full_name, title=title)
        self.session.add(executor)
        self.session.commit()
        self.record = executor

     def comments(self, comments):
        self.session.add(comments)
        self.session.commit()

URL = "sqlite://bd_opt.sqlite"
engine = sa.create_engine(URL)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()        

s = StorageManager()
s.register_document("X")

session.commit()
Base.metadata.create_all(engine)
session.close()